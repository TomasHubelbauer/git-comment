# Git Comment

A project for annotation source code files kept in Git with asides.
Each line has a unique ID: hash + file path + line number.
Annotations are either kept in Git objects, if I can figure out how.
(And how that would work with hosted Git and cloning and stuff.)
(Maybe Git Notes would be a good fit for this?)

There could be a Visual Studio Code plugin for pressing a shortcut while at a line.
It would open up an inline editor and allow to add a comment to that line or
range of lines, which would be displayed similarly to how blame information is displayed.

If possible, either keep the comments as user-specific Git objects or just
keep them out of the repo altogether.
Definitely cannot be tied to commits to the repository or anything team-facing like that.

## Running

Not yet.

## Contributing

Pick a better name?
